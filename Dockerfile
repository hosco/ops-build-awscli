FROM docker:stable

LABEL VERSION=1.0
LABEL DESCRIPTION="Image contains docker and aws cli"
LABEL maintainer="Hosco <dev@hosco.com>"

RUN apk add --no-cache bash git curl jq python py-pip
RUN pip install awscli
